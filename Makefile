APP_NAME=url-shortener
APP_ENV=local

SRC_DIR=./cmd/$(APP_NAME)
BUILD_DIR=./build

CONFIG_FILE=./config/$(APP_ENV).yaml

.PHONY: all
all: build run

.PHONY: build
build:
	@echo "Building $(APP_NAME)..."
	@go build -o $(BUILD_DIR)/$(APP_NAME) $(SRC_DIR)
	@echo "Done"
	@echo ""

.PHONY: run
run:
	@$(BUILD_DIR)/$(APP_NAME) --config=$(CONFIG_FILE)

.PHONY: clean
clean:
	@rm -rf $(BUILD_DIR)

.PHONY: test
test:
	@go test ./...