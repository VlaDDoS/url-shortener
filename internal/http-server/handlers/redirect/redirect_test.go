package redirect_test

import (
	"errors"
	"fmt"
	"github.com/go-chi/chi/v5"
	"net/http"
	"net/http/httptest"
	"testing"
	"url-shortener/internal/http-server/handlers/redirect"
	"url-shortener/internal/http-server/handlers/redirect/mocks"
	"url-shortener/internal/lib/logger/handlers/slogdiscard"
	"url-shortener/internal/storage"

	"github.com/stretchr/testify/require"
)

func TestRedirectHandler(t *testing.T) {
	cases := []struct {
		name        string
		alias       string
		status      int
		respError   string
		mockError   error
		redirectURL string
	}{
		{
			name:        "Success",
			alias:       "test_alias",
			status:      http.StatusFound,
			redirectURL: "https://google.com",
		},
		{
			name:      "Empty alias",
			alias:     "",
			status:    http.StatusNotFound,
			respError: "not found",
		},
		{
			name:      "URL not found",
			alias:     "not_found_alias",
			status:    http.StatusNotFound,
			respError: "not found",
			mockError: storage.ErrURLNotFound,
		},
		{
			name:      "GetURL Error",
			alias:     "test_alias",
			status:    http.StatusInternalServerError,
			respError: "failed to get url",
			mockError: errors.New("unexpected error"),
		},
	}

	for _, tc := range cases {
		tc := tc

		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			urlGetterMock := mocks.NewURLGetter(t)

			if tc.mockError != nil || tc.redirectURL != "" {
				urlGetterMock.On("GetURL", tc.alias).
					Return(tc.redirectURL, tc.mockError).
					Once()
			}

			handler := redirect.New(slogdiscard.NewDiscardLogger(), urlGetterMock)

			r := chi.NewRouter()
			r.Get("/{alias}", handler)

			input := fmt.Sprintf("/%s", tc.alias)

			req, err := http.NewRequest(http.MethodGet, input, nil)
			require.NoError(t, err)

			rr := httptest.NewRecorder()
			r.ServeHTTP(rr, req)

			require.Equal(t, rr.Code, tc.status)

			if tc.status != http.StatusFound {
				body := rr.Body.String()
				require.Contains(t, body, tc.respError)
			} else {
				require.Equal(t, tc.redirectURL, rr.Header().Get("Location"))
			}
		})
	}
}
