package random

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewRandomString(t *testing.T) {
	cases := []struct {
		name   string
		length int
	}{
		{
			name:   "size = 1",
			length: 1,
		},
		{
			name:   "size = 5",
			length: 5,
		},
		{
			name:   "size = 10",
			length: 10,
		},
		{
			name:   "size = 20",
			length: 20,
		},
		{
			name:   "size = 30",
			length: 30,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			str1 := NewRandomString(tt.length)
			str2 := NewRandomString(tt.length)

			assert.Len(t, str1, tt.length)
			assert.Len(t, str2, tt.length)

			assert.NotEqual(t, str1, str2)
		})
	}
}
